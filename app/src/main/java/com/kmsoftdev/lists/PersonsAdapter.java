package com.kmsoftdev.lists;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kmsoftdev.lists.databinding.CellPersonBinding;

import java.util.List;

public class PersonsAdapter extends RecyclerView.Adapter<PersonsAdapter.PersonVH> {
    private List<Person> persons;
    private LayoutInflater inflater;

    public PersonsAdapter(Context context, List<Person> persons) {
        this.persons = persons;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public PersonVH onCreateViewHolder(ViewGroup parent, int viewType) {

        CellPersonBinding binding = DataBindingUtil.inflate(inflater, R.layout.cell_person, parent, false);
        PersonVH holder = new PersonVH(binding.getRoot());
        holder.setBinding(binding);

        return holder;
    }

    @Override
    public void onBindViewHolder(PersonVH holder, int position) {
        final Person person = persons.get(position);
        holder.getBinding().setVariable(com.kmsoftdev.lists.BR.person, person);
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        if (persons != null) {
            return persons.size();
        }
        else return 0;
    }

    public void changeDataSet(List<Person> persons) {
        this.persons = persons;
        notifyDataSetChanged();
    }

    public class PersonVH extends RecyclerView.ViewHolder {
        private CellPersonBinding binding;

        public PersonVH(View itemView) {
            super(itemView);

        }

        public CellPersonBinding getBinding() {
            return binding;
        }

        public void setBinding(CellPersonBinding binding) {
            this.binding = binding;
        }
    }
}
