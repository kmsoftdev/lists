package com.kmsoftdev.lists;

import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuItem;

import com.kmsoftdev.lists.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        setSupportActionBar(mBinding.toolbar);

        setupRecycler();
    }

    private void setupRecycler() {
        if (isPortrait()) {
            mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        }
        else {
            mBinding.recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        }
        mBinding.recyclerView.setAdapter(new PersonsAdapter(this, getPersons()));
    }

    private boolean isPortrait() {
        return getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
    }

    private List<Person> getPersons() {
        List<Person> persons = new ArrayList<>(100);
        Person holder;
        for (int i = 0; i < 100; i++) {
            holder = new Person();
            holder.setName("Person " + i);
            holder.setAddress("Address " + i + "\n Address " + i + " line 2");
            persons.add(holder);
        }
        return persons;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
